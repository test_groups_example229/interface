/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.interfaceproject;

/**
 *
 * @author Tuf Gaming
 */
public class Snake extends Reptile {

    public Snake() {
        super("Snake", 0);
    }

    @Override
    public void eat() {
        System.out.println("Snake:eat");
    }

    @Override
    public void speak() {
        System.out.println("Snake:speak");
    }

    @Override
    public void sleep() {
          System.out.println("Snake:sleep");
    }

    @Override
    public void crawl() {
        System.out.println("Snake:crawl");
    }

}
