/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.interfaceproject;

/**
 *
 * @author Tuf Gaming
 */
public class Cat extends LandAnimal {

    public Cat() {
        super("Cat",4);
    }

    @Override
    public void eat() {
          System.out.println("Cat:eat");
    }

    @Override
    public void speak() {
          System.out.println("Cat:speak");
    }

    @Override
    public void sleep() {
          System.out.println("Cat:sleep");
    }

    @Override
    public void run() {
        System.out.println("Cat:run");
    }
    
}

    

