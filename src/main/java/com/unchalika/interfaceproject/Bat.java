/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.interfaceproject;

/**
 *
 * @author Tuf Gaming
 */
public class Bat extends Poultry {

    public Bat() {
        super();
    }

    @Override
    public void eat() {
        System.out.println("Bat:eat");
    }

    @Override
    public void speak() {
         System.out.println("Bat:speak");
    }

    @Override
    public void sleep() {
         System.out.println("Bat:sleep");
    }

    @Override
    public void fly() {
        System.out.println("Bat:fly");
    }

}
