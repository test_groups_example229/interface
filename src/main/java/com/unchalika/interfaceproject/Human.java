/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.interfaceproject;

/**
 *
 * @author Tuf Gaming
 */
public class Human extends LandAnimal {

    public Human() {
        super("Human", 2);
    }

        @Override
    public void eat() {
         System.out.println("Human:eat");
    }

    @Override
    public void speak() {
        System.out.println("Human:speak");
        
    }

    @Override
    public void sleep() {
        System.out.println("Human:sleep");
    }

    @Override
    public void run() {
        System.out.println("Human:run");
    }
    
}