/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.interfaceproject;

/**
 *
 * @author Tuf Gaming
 */
public class Fish extends AquaticAnimal{

    public Fish() {
        super("Fish",0);
    }

    @Override
    public void eat() {
         System.out.println("Fish:eat");
    }

    @Override
    public void speak() {
         System.out.println("Fish:speak");
    }

    @Override
    public void sleep() {
         System.out.println("Fish:sleep");
    }

    @Override
    public void swim() {
        System.out.println("Fish:swim");
    }
    
}

