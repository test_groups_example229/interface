/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.interfaceproject;

/**
 *
 * @author Tuf Gaming
 */
public class Crab extends AquaticAnimal{

    public Crab() {
        super("Crab",8);
    }

    @Override
    public void eat() {
          System.out.println("Crab:eat");
    }

    @Override
    public void speak() {
         System.out.println("Crab:speak");
    }

    @Override
    public void sleep() {
         System.out.println("Crab:sleep");
    }

    @Override
    public void swim() {
        System.out.println("Crab:swim");
    }
    
}


