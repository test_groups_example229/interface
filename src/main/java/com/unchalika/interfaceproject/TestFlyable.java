/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.interfaceproject;

/**
 *
 * @author Tuf Gaming
 */
public class TestFlyable {

    public static void main(String[] args) {
        Bat bat = new Bat();
        Plane plane = new Plane("Engine number1");
        bat.fly();//Animal,Poultry,Flyable
        plane.fly();//Vahicle,Flyable
        Dog dog = new Dog();
        Human human = new Human();
        Cat cat = new Cat();
        Snake snake = new Snake();
        Crocodie crocodie = new Crocodie();
        Fish fish = new Fish();
        Crab crab = new Crab();
        Flyable[] flyables = {bat, plane};
        for (Flyable f : flyables) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.statEngine();
                p.run();
            }
            f.fly();
        }
        Runable[] runables = {dog, plane, human, cat};
        for (Runable r : runables) {
            r.run();
          
                }
          Crawlable[] crawlable = {snake, crocodie};
            for (Crawlable c : crawlable) {
                c.crawl();
               
            }
 Swimable[] swimable = {fish,crab};
                for (Swimable s : swimable) {
                    s.swim();
        }

    }
}
